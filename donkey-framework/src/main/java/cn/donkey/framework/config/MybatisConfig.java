package cn.donkey.framework.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("cn.donkey.*.mapper.*Mapper")
public class MybatisConfig {
}
